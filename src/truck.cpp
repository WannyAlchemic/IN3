#include "truck.h"
#include <iostream>

Truck::Truck(std::string name, std::string manufacturer, std::string status, uint8_t seats, uint8_t axes, double cargo_capacity)
    : Vehicle(name, manufacturer, status)
    , seats_(seats)
    , axes_(axes)
    , cargo_capacity_(cargo_capacity)
{
    std::cout << "Truck: creating obj " << name_ << std::endl;
}

Truck::~Truck()
{
    std::cout << "Truck: destroying obj " << name_ << std::endl;
}

void Truck::print()
{
    Vehicle::print();

    std::cout << "Achsen: " << std::to_string(axes()) << std::endl;
    std::cout << "Kapazität: " << cargo_capacity() << std::endl;
}

void Truck::set_axes(int axes)
{
    axes_ = axes;
}