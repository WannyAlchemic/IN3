#ifndef _INPUT_H
#define _INPUT_H

#include <iostream>
#include <cstdint>
#include <string>
#include <type_traits>

template<typename T>
T get_number()
{
    std::string input;

    do {
        std::cin >> input;

        if (std::is_same<T, double>::value) {
            try {
                return stod(input);
            } catch (std::invalid_argument e) {
                std::cout << e.what() << std::endl;
                input = "";
            }
        } else {
            try {
                return stoi(input);
            } catch(std::invalid_argument e) {
                std::cout << e.what() << std::endl;
                input = "";
            }
        }
    } while (input.empty());
}


std::string get_text()
{
    std::string input;

    do {
        std::cin >> input;
    } while (input.empty());

    return input;
}

#endif