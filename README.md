# Case study 'StadtAuto'
## Topics
* inheritence, polymorphism, abstract classes, multiple inheritance
* Enum
* UML
* Software Life Cycle

## Building
```
$ make
```

## Execution
``
$ ./StadtAuto
``
