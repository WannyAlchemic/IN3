#ifndef _CAR_H
#define _CAR_H

#include "vehicle.h"
#include <cstdint>

class Car: public Vehicle
{
private:
    uint8_t seats_;
    std::string type_;
    bool sun_roof_;

public:
    Car(std::string name, std::string manufacturer, std::string status, uint8_t seats, std::string type, bool sun_roof);
    ~Car();

    uint8_t seats() { return seats_; }
    const std::string type() { return type_; }
    bool sun_roof() { return sun_roof_; }

    void print();
};

#endif