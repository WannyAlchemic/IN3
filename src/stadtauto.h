#ifndef _STADT_AUTO_H
#define _STADT_AUTO_H

#include "trailer.h"
#include "vehicle.h"
#include <array>
#include <cstdint>
#include <vector>
#include <memory>

class StadtAuto
{
private:
    std::vector<std::shared_ptr<Vehicle> > vehicles_;

public:
    StadtAuto() = default;
    ~StadtAuto() = default;

    void insert(std::string name, const std::string type, bool sun_roof, uint8_t seats, std::string status, const std::string manufacturer);
    void insert(std::string name, const std::string type, bool sun_roof, uint8_t seats, std::string status, const std::string manufacturer, uint8_t rooms, double square_meters, cat_t cat);
    void insert(std::string name, uint8_t axes, double cargo_capacity, uint8_t seats, std::string status, const std::string manufacturer);

    void print();
};

#endif