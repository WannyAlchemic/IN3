#ifndef _VEHICLE_H
#define _VEHICLE_H

#include <string>

class Vehicle
{
protected:
    std::string name_;
    std::string manufacturer_;
    std::string status_;

public:
    Vehicle() = default;
    Vehicle(std::string name, std::string manufacturer, std::string status);
    ~Vehicle();


    const std::string name() { return name_; }
    const std::string manufacturer() { return manufacturer_; }
    const std::string status() { return status_; }

    virtual void print();
    void change_status(std::string);
    void change_name(std::string);
};

#endif