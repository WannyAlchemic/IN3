#include "trailer.h"
#include <iostream>

Trailer::Trailer(std::string name, std::string manufacturer, std::string status, uint8_t seats, std::string type, bool sun_roof, uint8_t rooms, double square_meters, cat_t cat)
    : Car(name, manufacturer, status, seats, type, sun_roof)
    , Flat(rooms, square_meters)
    , cat_(cat)
{
    std::cout << "Trailer: creating obj " << name_ << std::endl;
}

Trailer::~Trailer()
{
    std::cout << "Trailer: destroying obj " << name_ << std::endl;
}

void Trailer::print()
{
    cat_t tmp_cat = cat();
    std::string tmp_string;

    Car::print();
    if (tmp_cat == 0) {
        tmp_string = "Luxus";
    } else if (tmp_cat == 1) {
        tmp_string = "Gehoben";
    } else if (tmp_cat == 2) {
        tmp_string = "Mittel";
    } else if (tmp_cat == 3) {
        tmp_string = "Einfach";
    }
    std::cout << "Kategorie: " << tmp_string << std::endl;
    Flat::print();
}

void Trailer::change_cat(cat_t cat)
{
    cat_ = cat;
}