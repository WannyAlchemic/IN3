#include "vehicle.h"
#include <iostream>

Vehicle::Vehicle(const std::string name, const std::string manufacturer, const std::string status)
    : name_(name)
    , manufacturer_(manufacturer)
    , status_(status)
{
    // std::cout << "Vehicle: creating obj " << name_ << std::endl;
}

Vehicle::~Vehicle()
{
    // std::cout << "Vehicle: destroying obj " << name_ << std::endl;
}

void Vehicle::print()
{
    std::cout << "Name: " << name() << std::endl;
    std::cout << "Hersteller: " << manufacturer() << std::endl;
    std::cout << "Status: " << status() << std::endl;
}

void Vehicle::change_status(std::string status)
{
    status_ = status;
}

void Vehicle::change_name(std::string name)
{
    name_ = name;
}