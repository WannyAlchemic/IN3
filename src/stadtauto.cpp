#include "stadtauto.h"
#include "car.h"
#include "truck.h"
#include <stdexcept>
#include <iostream>

//StadtAuto& operator<<(StadtAuto& stadtauto, Vehicle const& vehicle)
//{
//    stadtauto.insert(vehicle);
//}

void StadtAuto::insert(std::string name, const std::string type, bool sun_roof, uint8_t seats, std::string status, const std::string manufacturer)
{
    vehicles_.push_back(std::make_shared<Car>(name, manufacturer, status, seats, type, sun_roof));
}

void StadtAuto::insert(std::string name, const std::string type, bool sun_roof, uint8_t seats, std::string status, const std::string manufacturer, uint8_t rooms, double square_meters, cat_t cat)
{
    vehicles_.push_back(std::make_shared<Trailer>(name, manufacturer, status, seats, type, sun_roof, rooms, square_meters, cat));
}

void StadtAuto::insert(std::string name, uint8_t axes, double cargo_capacity, uint8_t seats, std::string status, const std::string manufacturer)
{
    vehicles_.push_back(std::make_shared<Truck>(name, manufacturer, status, seats, axes, cargo_capacity));
}

void StadtAuto::print()
{
    std::cout << "\t*** Übersicht des StadtAuto-Fuhrparks ***" << std::endl;
    for (auto vehicle: vehicles_) {
        std::cout << "--------------------------" << std::endl;
        vehicle->print();
        std::cout << "--------------------------" << std::endl;
    }

}