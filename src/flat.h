#ifndef _FLAT_H
#define _FLAT_H

#include <cstdint>

class Flat
{
private:
    uint8_t rooms_;
    double square_meters_;

public:
    Flat(uint8_t rooms, double square_meters);
    ~Flat();

    uint8_t rooms() { return rooms_; }
    double square_meters() { return square_meters_; }

    void print();
};

#endif