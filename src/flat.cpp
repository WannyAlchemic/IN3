#include "flat.h"
#include <iostream>

Flat::Flat(uint8_t rooms, double square_meters)
    : rooms_(rooms)
    , square_meters_(square_meters)
{
    // std::cout << "Flat: creating obj " << square_meters_ << std::endl;
}

Flat::~Flat()
{
    // std::cout << "Flat: destroying obj " << square_meters_ << std::endl;
}

void Flat::print()
{
    std::cout << "Räume: " << rooms() << std::endl;
    std::cout << "Quadratmeter: " << square_meters() << std::endl;
}