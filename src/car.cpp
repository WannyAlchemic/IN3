#include "car.h"
#include <iostream>

Car::Car(std::string name, std::string manufacturer, std::string status, uint8_t seats, std::string type, bool sun_roof)
    : Vehicle(name, manufacturer, status)
    , seats_(seats)
    , type_(type)
    , sun_roof_(sun_roof)
{
    std::cout << "Car: creating obj " << name_ << std::endl;
}

Car::~Car()
{
    std::cout << "Car: destroying obj " << name_ << std::endl;
}

void Car::print()
{
    std::string roof;

    Vehicle::print();

    std::cout << "Sitze: " << std::to_string(seats()) << std::endl;
    std::cout << "Typ: " << type() << std::endl;

    if (sun_roof() == 1) {
        roof = "ja";
    } else {
        roof = "nein";
    }
    std::cout << "Sonnendach: " << roof << std::endl;
}