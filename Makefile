CC = ccache g++
LDFLAGS=
CFLAGS += -Wall -Wextra -pedantic
CFLAGS += -std=c++14
CFLAGS += -g

OBJS:=$(wildcard src/*.cpp)
OBJS:=$(OBJS:.cpp=.o)
PROG=StadtAuto


%.o: %.cpp %.h
	$(CC) $(CFLAGS) -c -o $@ $<
	@echo " CC $<"

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $^
	@echo " LD $@"

clean:
	@rm -f src/*.o $(PROG)
