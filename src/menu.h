#ifndef _MENU_H
#define _MENU_H

#include "stadtauto.h"
#include "input.h"
#include <cstdint>
#include <string>
#include <iostream>
#include <cstdlib>
#include <thread>

#ifdef __APPLE__ || __linux__
    #define CLEAR system("clear")
#else
    #define CLEAR system("CLS")
#endif

void clear_buffer()
{
    uint8_t ch;
    while ((ch = getc(stdin)) != '\n' && ch != EOF);
}

char print_top_menu()
{
    char c;
    char ch;

    do {
        CLEAR;

        std::cout << "\t*** Verwaltung des StadtAuto-Fuhrparks ***" << std::endl;
        std::cout << "P = Pkw neu aufnehmen" << std::endl;
        std::cout << "L = Lkw neu aufnehmen" << std::endl;
        std::cout << "W = Wohnwagen neu aufnehmen" << std::endl;
        std::cout << "A = Fuhrpark anzeigen" << std::endl;
        std::cout << "B = Programm beenden" << std::endl;

        std::cout << "Ihre Auswahl: ";
        std::cin >> c;

    } while (c != 'p' && c != 'P' && c != 'l' && c != 'L' && c != 'a' && c != 'A' && c != 'b' && c != 'B' && c != 'w' && c != 'W');

    return c;
}

void vehicle_menu(StadtAuto& stadtauto, std::string vehicle_type)
{
    CLEAR;
    std::cout << "\t*** Verwaltung des StadtAuto-Fuhrparks ***" << std::endl;

    char tmp;

    if (vehicle_type == "car") {
        std::string car_name, car_manufacturer, car_status, car_type;
        int seats;
        bool sun_roof;

        std::cout << "Name: ";
        car_name = get_text();

        std::cout << "Hersteller: ";
        car_manufacturer = get_text();

        std::cout << "Status: ";
        car_status = get_text();

        std::cout << "Anzahl d. Sitze: ";
        seats = get_number<int>();

        std::cout << "Typ: ";
        car_type = get_text();

        std::cout << "Sonnendach vorhanden (j/n): ";
        std::cin >> tmp;

        if (tmp == 'j' || tmp == 'J') {
            sun_roof = true;
        } else {
            sun_roof = false;
        }

        stadtauto.insert(car_name, car_type, sun_roof, seats, car_status, car_manufacturer);

    } else if (vehicle_type == "truck") {
        std::string truck_name, truck_manufacturer, truck_status, truck_type;
        double cargo_capacity;
        int seats;
        int axes;

        std::cout << "Name: ";
        truck_name = get_text();

        std::cout << "Hersteller: ";
        truck_manufacturer = get_text();

        std::cout << "Status: ";
        truck_status = get_text();

        std::cout << "Anzahl d. Sitze: ";
        seats = get_number<int>();

        std::cout << "Anzahl d. Achsen: ";
        axes = get_number<int>();

        std::cout << "Kapazität: ";
        cargo_capacity = get_number<double>();

        stadtauto.insert(truck_name, axes, cargo_capacity, seats, truck_status, truck_manufacturer);

    } else if (vehicle_type == "trailer") {
        std::string trailer_name, trailer_manufacturer, trailer_status, trailer_type;
        int seats;
        int rooms;
        double square_meters;
        cat_t cat;
        bool sun_roof;
        std::string tmp_cat;

        std::cout << "Name: ";
        trailer_name = get_text();

        std::cout << "Hersteller: ";
        trailer_manufacturer = get_text();

        std::cout << "Status: ";
        trailer_status = get_text();

        std::cout << "Anzahl d. Sitze: ";
        seats = get_number<int>();

        std::cout << "Typ: ";
        trailer_type = get_text();

        std::cout << "Sonnendach vorhanden (j/n): ";
        std::cin >> tmp;

        if (tmp == 'j' || tmp == 'J') {
            sun_roof = true;
        } else {
            sun_roof = false;
        }

        std::cout << "Anzahl d. Räume: ";
        rooms = get_number<int>();

        std::cout << "Quadratmeter: ";
        square_meters = get_number<double>();

        std::cout << "Kategorie: ";
        tmp_cat = get_text();

        if (tmp_cat == "Luxus" || tmp_cat == "luxus") {
            cat = Luxus;
        } else if (tmp_cat == "Gehoben" || tmp_cat == "gehoben") {
            cat = Gehoben;
        } else if (tmp_cat == "Mittel" || tmp_cat == "mittel") {
            cat = Mittel;
        } else if (tmp_cat == "Einfach" || tmp_cat == "einfach") {
            cat = Einfach;
        }

        stadtauto.insert(trailer_name, trailer_type, sun_roof, seats, trailer_status, trailer_manufacturer, rooms, square_meters, cat);
    }
}

void create_menu(StadtAuto& stadtauto)
{
    char ch;

    while (true) {
        char ch = print_top_menu();
        switch (ch) {
            case 'b':
            case 'B':
                exit(EXIT_SUCCESS);
            case 'p':
            case 'P':
                vehicle_menu(stadtauto, "car");
                break;
            case 'l':
            case 'L':
                vehicle_menu(stadtauto, "truck");
                break;
            case 'w':
            case 'W':
                vehicle_menu(stadtauto, "trailer");
                break;
            case 'a':
            case 'A':
                {
                    CLEAR;
                    stadtauto.print();
                    clear_buffer();

                    std::cout << "\nBitte Enter Taste drücken..." << std::endl;
                    getchar();

                    // std::chrono::seconds sec(5);
                    // std::this_thread::sleep_for(sec);
                    break;
                }
            default:
                break;
        }
    }
}

#endif