#ifndef _TRUCK_H
#define _TRUCK_H

#include "vehicle.h"
#include <cstdint>

class Truck: public Vehicle
{
private:
    uint8_t seats_;
    uint8_t axes_;
    double cargo_capacity_;

public:
    Truck(std::string name, std::string manufacturer, std::string status, uint8_t seats, uint8_t axes, double cargo_capacity);
    ~Truck();

    uint8_t seats() { return seats_; }
    int axes() { return axes_; }
    double cargo_capacity() { return cargo_capacity_; }

    void print();
    void set_axes(int);
};

#endif