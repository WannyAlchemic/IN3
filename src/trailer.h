#ifndef _TRAILER_H
#define _TRAILER_H

#include "car.h"
#include "flat.h"

enum cat {Luxus, Gehoben, Mittel, Einfach};
typedef cat cat_t;

class Trailer: public Car, public Flat
{
private:
    cat_t cat_;

public:
    Trailer(std::string name, std::string manufacturer, std::string status, uint8_t seats, std::string type, bool sun_roof, uint8_t rooms, double square_meters, cat_t cat);
    ~Trailer();

    cat_t cat() { return cat_; }

    void print();
    void change_cat(cat_t cat);
};

#endif